#Manually install Xcode utils first.
#Also, manually install java 1.6 and 1.7

#Manually run this after starting alfred:
#brew cask alfred link

sudo softwareupdate -i -a

ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

brew doctor

brew install caskroom/cask/brew-cask

#http://www.topbug.net/blog/2013/04/14/install-and-use-gnu-command-line-tools-in-mac-os-x/
brew install coreutils

#override with GNU
brew tap homebrew/dupes
brew install binutils
brew install diffutils
brew install ed --default-names
brew install findutils --default-names
brew install gawk
brew install gnu-indent --default-names
brew install gnu-sed --default-names
brew install gnu-tar --default-names
brew install gnu-which --default-names
brew install gnutls --default-names
brew install grep --default-names
brew install gzip
brew install screen
brew install watch
brew install wdiff --with-gettext
brew install wget




#security upgrade
brew install bash

#various non-GNU upgrades
brew install file-formula
brew install git
brew install less
brew install openssh --with-brewed-openssl
brew install python --with-brewed-openssl
brew install rsync
brew install unzip
brew install vim --override-system-vi
#not install macvim, don't feel like installing all of x-code
#brew install macvim --override-system-vim --custom-system-icons
brew install zsh





#Various stuff that I want
brew install ssh-copy-id
brew install hub
brew install ack
brew install bash-completion
brew install rename
brew install tree
brew install watch
brew install osxutils
brew install p7zip
brew install maven
brew install tmux
#brew install todo-txt #not maintained, very old version in home brew
brew intall htop
brew install dos2unix
brew install ghi


#cask installs
brew cask install copy
brew cask install firefox
brew cask install filezilla
brew cask install iterm2
brew cask install chromium 
brew cask install skype 
brew cask install textwrangler
brew cask install intellij-idea-ce
brew cask install vlc
brew cask install sublime-text
#Note, this is not installed automatically, requires running an installer afterwards lol… pointless cask.
#brew cask install dbvisualizer
brew cask install flux
brew cask install alfred
brew cask install gitx
brew cask install appcleaner
brew cask install scroll-reverser
brew cask install keepassx

#try out:
brew cask install notational-velocity
brew cask install dropzone
brew cask install hyperdock
brew cask install transmit
brew cask install daisydisk
#brew cask install sizeup -or- brew cask install divvy -or- brew cask install shiftit
brew cask install cd-to
brew cask install todotxtmac

#fonts
brew cask install font-inconsolata
brew cask install font-source-code-pro
brew cask install font-fira-sans
brew cask install font-ubuntu

# Clean things up
brew linkapps
brew cleanup
brew prune
brew cask cleanup



# htop-osx requires root privileges to correctly display all running processes.
sudo chown root:wheel "$(brew --prefix)/bin/htop"
sudo chmod u+s "$(brew --prefix)/bin/htop"

#patch up man pages
# Short of learning how to actually configure OSX, here's a hacky way to use
# GNU manpages for programs that are GNU ones, and fallback to OSX manpages otherwise
alias man='_() { echo $1; man -M $(brew --prefix)/opt/coreutils/libexec/gnuman $1 1>/dev/null 2>&1;  if [ "$?" -eq 0 ]; then man -M $(brew --prefix)/opt/coreutils/libexec/gnuman $1; else man $1; fi }; _'






#install homeshick for dotfiles
git clone git://github.com/andsens/homeshick.git $HOME/.homesick/repos/homeshick

source "$HOME/.homesick/repos/homeshick/homeshick.sh"
homeshick clone jimdeanspivey/dotfiles
homeshick symlink dotfiles


#Ensure that the dotfiles for bash and zsh have these PATH’s setup,

#export PATH=$(brew --prefix coreutils)/libexec/gnubin:/usr/local/bin:$PATH


#TODO: git script to clone power line fonts and run the installer + remove from home directory of dot files
