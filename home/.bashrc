
#Only invoke .bashrc customizations if in an interactive shell 
if [ -z "$PS1" ]; then
      return;
fi

#bashrc

export PATH=$PATH:$HOME/scripts

# set 256 color profile where possible
if [[ $COLORTERM == gnome-* && $TERM == xterm ]] && infocmp gnome-256color >/dev/null 2>&1; then
    export TERM=gnome-256color
elif infocmp xterm-256color >/dev/null 2>&1; then
    export TERM=xterm-256color
fi

shopt -s cdspell
shopt -s checkwinsize
shopt -s cmdhist
shopt -s dotglob
shopt -s expand_aliases
shopt -s extglob
shopt -s histappend
shopt -s hostcomplete
shopt -s nocaseglob

#load dotfiles as modules (each dotfile manages its own order and depdencies)
. $HOME/.bash/exports.bash
. $HOME/.bash/aliases.bash
. $HOME/.bash/appearance/prompt.bash
. $HOME/.bash/osx.bash
. $HOME/.bash/linux.bash

eval "$(dircolors -b "$HOME"/.bash/appearance/dircolors.bash)"

# Load custom bash completions
for f in $HOME/.bash/completion/*.bash; do
    . "$f"
done

#Machine specific
if [ -f $HOME/.local.bash ]; then
    . "$HOME"/.local.bash
fi

#Colorize man
man() {
    env \
        LESS_TERMCAP_mb="$(printf "\e[1;31m")" \
        LESS_TERMCAP_md="$(printf "\e[1;31m")" \
        LESS_TERMCAP_me="$(printf "\e[0m")" \
        LESS_TERMCAP_se="$(printf "\e[0m")" \
        LESS_TERMCAP_so="$(printf "\e[1;44;33m")" \
        LESS_TERMCAP_ue="$(printf "\e[0m")" \
        LESS_TERMCAP_us="$(printf "\e[1;32m")" \
            man "$@"
}
