export HISTSIZE=10000
export HISTFILESIZE=${HISTSIZE}
export HISTCONTROL=ignoreboth

export GREP_COLOR='1;32'

export LS_COLORS='di=1:fi=0:ln=31:pi=5:so=5:bd=5:cd=5:or=31:*.deb=90'
export LS_OPTIONS='--group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color -F --block-size=K'

export VISUAL=vim #override this setting in local.bash to nvim
export EDITOR=vim

# Really a sensible default. When it's not set, it leads to errors
export MAVEN_OPTS="-Xmx2g -XX:MaxPermSize=4096M -XX:ReservedCodeCacheSize=1024m"
#Hash the hostname to number, then has that number into a bash supported color value
export HOSTNAME=`hostname`
export HOSTNAME_HASHED=`python2 -c 'from sys import argv;print(hash(argv[1]))' $HOSTNAME`
export HOSTNAME_COLOR_CODE=$((HOSTNAME_HASHED % 20 + 99))

