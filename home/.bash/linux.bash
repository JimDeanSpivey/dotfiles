#Linux
if [[ "$OSTYPE" == "linux-gnu" ]]; then

  #Setup Java
  javasethome() {
    export JAVA_HOME
    JAVA_HOME=$(whereis javac | cut -d' ' -f2 | egrep -o  '(\/[a-zA-Z0-9]*){1,}\/')
  }
fi
