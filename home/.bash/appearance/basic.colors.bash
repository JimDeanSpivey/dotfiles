RED="$(tput setaf 1)"
GREEN="$(tput setaf 2)"
YELLOW="$(tput setaf 3)"
BLUE="$(tput setaf 4)"
PURPLE="$(tput setaf 5)"
CYAN="$(tput setaf 6)"
WHITE="$(tput setaf 7)"
ORANGE="$(tput setaf 208)"

BRIGHT_RED="$(tput setaf 9)"
BRIGHT_GREEN="$(tput setaf 10)"
BRIGHT_YELLOW="$(tput setaf 11)"
BRIGHT_BLUE="$(tput setaf 12)"
BRIGHT_PURPLE="$(tput setaf 13)"
BRIGHT_CYAN="$(tput setaf 14)"
BRIGHT_WHITE="$(tput setaf 15)"

RESET="$(tput sgr0)"       # text reset
BOLD="$(tput bold)"       # make bold
undrln="$(tput smul)"       # underline
noundr="$(tput rmul)"       # remove underline
mkblnk="$(tput blink)"      # make blink
revers="$(tput rev)"        # reverse
