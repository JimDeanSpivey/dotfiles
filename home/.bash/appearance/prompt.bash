. $HOME/.bash/appearance/basic.colors.bash


parse_git_dirty () {
  [[ $(git status 2> /dev/null | tail -n1) != "nothing to commit"* ]] && echo "*"
}
parse_git_branch () {
  git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e "s/* \(.*\)/\1$(parse_git_dirty)/"
}
 
PS1="\[${BOLD}${CYAN}\]\u \[$BASE0\]at \[$CYAN\]\h \[$BASE0\]in \[$BLUE\]\w\[$BASE0\]\$([[ -n \$(git branch 2> /dev/null) ]] && echo \" on \")\[$YELLOW\]\$(parse_git_branch)\[$BASE0\]\n\$ \[$RESET\]"

USERCOLOR=$(tput setaf $HOSTNAME_COLOR_CODE) 

if [ "$USER" == "root" ]; then
    USERCOLOR=$RED
fi

PS1="\[${BOLD}${USERCOLOR}\]\u@\h   \[$GREEN\]\w\[$WHITE\]\$([[ -n \$(git branch 2> /dev/null) ]] && echo \"   \")\[$BLUE\]\$(parse_git_branch)\[$WHITE\]\n\$ \[$RESET\]"
