#!/bin/sh

# colors from solarized: http://ethanschoonover.com/solarized
count="$(tput colors)"

# shellcheck disable=SC2034
if [ "$count" = "256" ]; then
  base00="$(tput setaf 241)"
  base01="$(tput setaf 240)"
  base02="$(tput setaf 235)"
  base03="$(tput setaf 234)"
  base0="$(tput setaf 244)"
  base1="$(tput setaf 245)"
  base2="$(tput setaf 254)"
  base3="$(tput setaf 230)"
  blue="$(tput setaf 33)"
  cyan="$(tput setaf 37)"
  green="$(tput setaf 64)"
  magenta="$(tput setaf 125)"
  orange="$(tput setaf 166)"
  red="$(tput setaf 160)"
  violet="$(tput setaf 61)"
  yellow="$(tput setaf 136)"
elif [ "$count" -gt 8 ]; then
  base00="$(tput setaf 11)"
  base01="$(tput setaf 10)"
  base02="$(tput setaf 0)"
  base03="$(tput setaf 8)"
  base0="$(tput setaf 12)"
  base1="$(tput setaf 14)"
  base2="$(tput setaf 7)"
  base3="$(tput setaf 15)"
  blue="$(tput setaf 4)"
  cyan="$(tput setaf 6)"
  green="$(tput setaf 2)"
  magenta="$(tput setaf 5)"
  orange="$(tput setaf 9)"
  red="$(tput setaf 1)"
  violet="$(tput setaf 13)"
  yellow="$(tput setaf 3)"
else
  base00="$(tput setaf 7)"
  base01="$(tput setaf 7)"
  base02="$(tput setaf 4)"
  base03="$(tput setaf 4)"
  base0="$(tput setaf 6)"
  base1="$(tput setaf 4)"
  base2="$(tput setaf 7)"
  base3="$(tput setaf 7)"
  blue="$(tput setaf 4)"
  cyan="$(tput setaf 6)"
  green="$(tput setaf 2)"
  magenta="$(tput setaf 5)"
  orange="$(tput setaf 3)"
  red="$(tput setaf 1)"
  violet="$(tput setaf 5)"
  yellow="$(tput setaf 3)"
fi

# shellcheck disable=SC2034
standout="$(tput smso)"
# shellcheck disable=SC2034
resetcolors="$(tput sgr0)"

# vim: set ft=sh: