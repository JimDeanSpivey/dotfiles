#Mac
if [[ "$OSTYPE" == "darwin"* ]]; then


    [[ -r "/usr/local/etc/profile.d/bash_completion.sh" ]] && . "/usr/local/etc/profile.d/bash_completion.sh"

  test -e "${HOME}"/.iterm2_shell_integration.bash && source "${HOME}"/.iterm2_shell_integration.bash

    #Java
    javasethome() {
        #Format of args is 1.5, 1.6, 1.7, 1.8
        export JAVA_HOME
        JAVA_HOME="$(/usr/libexec/java_home -v "$1")"
    }
    #set this in ~/.local.bash
    #javasethome 1.7

    #Fix annoying AWT window popup when running mvn
    export JAVA_TOOL_OPTIONS='-Djava.awt.headless=true'

    #Android
    export ANDROID_HOME=/usr/local/opt/android-sdk

    #Homebrew fixes for gnu-utils and gnu man pages
    export PATH
    PATH="$(brew --prefix coreutils)"/libexec/gnubin:/usr/local/bin:$PATH
    PATH="/usr/local/opt/grep/libexec/gnubin:$PATH"
    export MANPATH
    MANPATH="/usr/local/opt/coreutils/libexec/gnuman:$MANPATH"
fi
