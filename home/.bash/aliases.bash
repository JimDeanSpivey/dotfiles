# Note on aliases, strings defined with single quotes are literals, but they
# will be evaluated when the alias is ran. See:
# https://github.com/koalaman/shellcheck/wiki/SC2139

alias ls="ls $LS_OPTIONS"
alias ll="ls -l $LS_OPTIONS"
alias la="ls -la $LS_OPTIONS"
alias grep='grep --color=tty -d skip'
alias cp='cp -i'                          # confirm before overwriting something
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB
alias mutt="mutt -e 'set content_type=text/html'"

#Vim
alias vim='$EDITOR'
alias vr='$EDITOR -R'

#Java
alias mci='mvn clean install -Dmaven.test.skip=true'
alias mi='mvn install -Dmaven.test.skip=true'
alias gb='gradle build --daemon' #This isn't really that helpful, it makes running gradle clean build impossible
#Note: 'javasethome' is a custom function in osx.bash
alias gb8='javasethome 1.8 && gb'

alias g=git
complete -F _git g
alias gh=hub
alias gst='git status'
alias ga='git add'
alias gc='git commit'

alias rb='ruby'

alias k8=kubectl
alias kk='kubectl'
alias kd='kubectl describe'
alias kkl='kubectl logs'
alias kkd='kubectl delete'
alias kkg='kubectl get'

# quickly modifiy dotfile config
# Note: I don't want to export EDIT_HOME_BASH and this looks cleaner with double quotes
EDIT_HOME_BASH="$EDITOR $HOME/.bash"
alias cdflocal="$EDITOR $HOME/.local.bash"
alias cdfaliases="$EDIT_HOME_BASH/aliases.bash"
alias cdfexports="$EDIT_HOME_BASH/exports.bash"
alias cdfosx="$EDIT_HOME_BASH/osx.bash"
alias cdflinux="$EDIT_HOME_BASH/linux.bash"
alias cdfprompt="$EDIT_HOME_BASH/appearance/prompt.bash"
alias cdfbashrc="$EDITOR $HOME/.bashrc"
alias cdfvimrc="$EDITOR $HOME/.vimrc"
