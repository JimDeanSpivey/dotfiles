dotfiles
========

It's mostly homeshick driven. Obviously anything in the home directory [is symlinked](https://github.com/andsens/homeshick/wiki/Symlinking).


Bash
----

bash config is stored under `~/.bash/`. It is mostly self made but obviosuly there are recurring patterns and lots of copied files from other projects:

 * [Debian's bash-completion](https://bash-completion.alioth.debian.org/). Instead of using init system, it just sources the file from .bashrc, which is nice. Consideing that homeshick imports this into a lot of different systems.
 * a select few [bash completion scripts from bash-it](https://github.com/revans/bash-it/tree/master/completion/available).
 * [prompt.bash](https://github.com/JimDeanSpivey/dotfiles/blob/master/home/.bash/appearance/prompt.bash) contains a neat feature. It computes a hash of the hostname so that different systems have different color PS1 prompts.
 * [osx has special configurations](https://github.com/JimDeanSpivey/dotfiles/blob/master/home/.bash/osx.bash) to use gnu-utils instead of the default BSD commands. It also fixes up how java is configured on the path.
 * It uses the amazing [LS_COLORS configuration](https://github.com/trapd00r/LS_COLORS)
 * custom scripts are in the 'scripts' folder, which is on the `$PATH`



TODO
------------------

 * Will probably delete the iterm color schemes. I don't see the value of versioning the color schemes in git. These files are backed up elsewhere. Either that, or I will make a new (non homeshick) repo for mac files.
 * New to vim, will enhance/organize existing vimrc or start with a new copy.
 * May look into modifying bash's keybindings, but probably won't.
 * A lot more work (but almost done) on karabiner key files.
